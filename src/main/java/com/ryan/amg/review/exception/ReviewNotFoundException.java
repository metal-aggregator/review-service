package com.ryan.amg.review.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class ReviewNotFoundException extends RuntimeException {

    private static final String ERROR_MESSAGE_TEMPLATE = "The review with reviewId=%s was not found.";

    public ReviewNotFoundException(String reviewId) {
        super(String.format(ERROR_MESSAGE_TEMPLATE, reviewId));
    }
}
