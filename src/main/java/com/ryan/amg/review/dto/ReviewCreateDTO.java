package com.ryan.amg.review.dto;

import com.ryan.amg.review.domain.AlbumReview;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReviewCreateDTO {
    private String band;
    private String album;
    private String url;
    private List<String> tags = new ArrayList<>();
    private List<String> genres = new ArrayList<>();
    private List<String> relatedBands = new ArrayList<>();
    private String score;
    private String imageUrl;

    public AlbumReview toDomain() {
        return new AlbumReview(null, band, album, url, new ArrayList<>(tags), new ArrayList<>(genres), new ArrayList<>(relatedBands), score, false, imageUrl, null, false);
    }
}
