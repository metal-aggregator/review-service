package com.ryan.amg.review.dto;

import com.ryan.amg.review.domain.AlbumReview;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

@Data
@NoArgsConstructor
public class AlbumReviewDTO {
    private String id;
    private String band;
    private String album;
    private String url;
    private List<String> tags;
    private List<String> genres;
    private List<String> relatedBands;
    private String score;
    private boolean processed;
    private String imageUrl;
    private LocalDateTime createdDate;

    public AlbumReviewDTO(AlbumReview albumReview) {
        this.id = albumReview.getId();
        this.band = albumReview.getBand();
        this.album = albumReview.getAlbum();
        this.url = albumReview.getUrl();
        this.tags = albumReview.getTags();
        this.genres = albumReview.getGenres();
        this.relatedBands = albumReview.getRelatedBands();
        this.score = albumReview.getScore();
        this.processed = albumReview.isProcessed();
        this.imageUrl = albumReview.getImageUrl();
        this.createdDate = albumReview.getCreatedDate();
    }

}
