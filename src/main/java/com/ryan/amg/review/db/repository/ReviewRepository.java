package com.ryan.amg.review.db.repository;

import com.ryan.amg.review.db.entity.ReviewEntity;
import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

@EnableScan
public interface ReviewRepository extends CrudRepository<ReviewEntity, String> {
    List<ReviewEntity> findByUrl(String url);
}
