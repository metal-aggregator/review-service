package com.ryan.amg.review.controller;

import com.ryan.amg.review.dto.AlbumReviewDTO;
import com.ryan.amg.review.service.ReviewService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/reviews/{reviewId}/genres")
@AllArgsConstructor
public class GenreController {

    private final ReviewService reviewService;

    @PutMapping(value = "/{genre}")
    public AlbumReviewDTO addGenreToReview(@PathVariable String reviewId, @PathVariable String genre) {
        return new AlbumReviewDTO(reviewService.addGenreToReview(reviewId, genre));
    }

}
