package com.ryan.amg.review.controller;

import com.ryan.amg.review.dto.AlbumReviewDTO;
import com.ryan.amg.review.service.ReviewService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/reviews/{reviewId}/score")
@AllArgsConstructor
public class ScoreController {

    private final ReviewService reviewService;

    @PutMapping(value = "/{score}")
    public AlbumReviewDTO addScoreToReview(@PathVariable String reviewId, @PathVariable String score) {
        return new AlbumReviewDTO(reviewService.addScoreToReview(reviewId, score));
    }

}
