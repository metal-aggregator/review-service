package com.ryan.amg.review.controller;

import com.ryan.amg.review.dto.AlbumReviewDTO;
import com.ryan.amg.review.service.ReviewService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/reviews/{reviewId}/relatedBands")
@AllArgsConstructor
public class RelatedBandController {

    private final ReviewService reviewService;

    @PutMapping(value = "/{relatedBand}")
    public AlbumReviewDTO addRelatedBandToReview(@PathVariable String reviewId, @PathVariable String relatedBand) {
        return new AlbumReviewDTO(reviewService.addRelatedBandToReview(reviewId, relatedBand));
    }

}
