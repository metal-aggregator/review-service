package com.ryan.amg.review.controller;

import com.ryan.amg.review.dto.AlbumReviewDTO;
import com.ryan.amg.review.service.ReviewService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/reviews/{reviewId}/processed")
@AllArgsConstructor
public class ProcessedController {

    private final ReviewService reviewService;

    @PutMapping(value = "/{processedFlag}")
    public AlbumReviewDTO toggleReviewAsProcessed(@PathVariable String reviewId, @PathVariable boolean processedFlag) {
        return new AlbumReviewDTO(reviewService.toggleReviewAsProcessed(reviewId, processedFlag));
    }

}
