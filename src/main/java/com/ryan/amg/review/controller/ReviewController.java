package com.ryan.amg.review.controller;

import com.ryan.amg.review.domain.AlbumReview;
import com.ryan.amg.review.dto.AlbumReviewDTO;
import com.ryan.amg.review.dto.ReviewCreateDTO;
import com.ryan.amg.review.service.ReviewService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/reviews")
@AllArgsConstructor
public class ReviewController {

    private final ReviewService reviewService;

    @PostMapping
    public ResponseEntity<AlbumReviewDTO> createReview(@RequestBody ReviewCreateDTO reviewCreateDTO) {
        AlbumReview albumReview = reviewService.createAlbumReview(reviewCreateDTO.toDomain());
        HttpStatus resultStatus = albumReview.isCreated() ? HttpStatus.CREATED : HttpStatus.OK;
        return new ResponseEntity<>(new AlbumReviewDTO(albumReview), resultStatus);
    }

    @DeleteMapping(value = "/{reviewId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteReview(@PathVariable String reviewId) {
        reviewService.deleteAlbumReview(reviewId);
    }

}
