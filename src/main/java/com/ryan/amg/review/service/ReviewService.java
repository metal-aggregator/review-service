package com.ryan.amg.review.service;

import com.ryan.amg.review.db.entity.ReviewEntity;
import com.ryan.amg.review.db.repository.ReviewRepository;
import com.ryan.amg.review.domain.AlbumReview;
import com.ryan.amg.review.exception.ReviewNotFoundException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Component
@AllArgsConstructor
public class ReviewService {

    private final ReviewRepository reviewRepository;

    public AlbumReview createAlbumReview(AlbumReview albumReview) {
        List<ReviewEntity> existingReviewEntities = reviewRepository.findByUrl(albumReview.getUrl());
        AlbumReview resultingAlbumReview;
        if (!CollectionUtils.isEmpty(existingReviewEntities)) {
            resultingAlbumReview = existingReviewEntities.get(0).toDomain();
            resultingAlbumReview.setCreated(false);
        } else {
            ReviewEntity newReviewEntity = new ReviewEntity(albumReview);
            newReviewEntity.setCreatedDate(LocalDateTime.now());
            resultingAlbumReview = reviewRepository.save(newReviewEntity).toDomain();
            resultingAlbumReview.setCreated(true);
        }
        return resultingAlbumReview;
    }

    public AlbumReview addGenreToReview(String reviewId, String genre) {
        ReviewEntity entity = getReviewEntityByIdOrThrowIfNotFound(reviewId);
        entity.addGenre(genre);
        return reviewRepository.save(entity).toDomain();
    }

    public AlbumReview addRelatedBandToReview(String reviewId, String relatedBand) {
        ReviewEntity entity = getReviewEntityByIdOrThrowIfNotFound(reviewId);
        entity.addRelatedBand(relatedBand);
        return reviewRepository.save(entity).toDomain();
    }

    public AlbumReview addScoreToReview(String reviewId, String score) {
        ReviewEntity entity = getReviewEntityByIdOrThrowIfNotFound(reviewId);
        entity.setScore(score);
        return reviewRepository.save(entity).toDomain();
    }

    public AlbumReview toggleReviewAsProcessed(String reviewId, boolean processed) {
        ReviewEntity entity = getReviewEntityByIdOrThrowIfNotFound(reviewId);
        entity.setProcessed(processed);
        return reviewRepository.save(entity).toDomain();
    }

    public void deleteAlbumReview(String reviewId) {
        reviewRepository.deleteById(reviewId);
    }

    private ReviewEntity getReviewEntityByIdOrThrowIfNotFound(String reviewId) {
        Optional<ReviewEntity> existingReviewEntity = reviewRepository.findById(reviewId);
        if (existingReviewEntity.isPresent()) {
            return existingReviewEntity.get();
        }
        throw new ReviewNotFoundException(reviewId);

    }

}
