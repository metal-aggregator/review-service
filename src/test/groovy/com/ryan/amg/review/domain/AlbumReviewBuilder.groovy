package com.ryan.amg.review.domain

import com.ryan.amg.review.db.entity.ReviewEntity

import java.time.LocalDateTime

class AlbumReviewBuilder {
    String id = 'id-12345'
    String band = 'Amon Amarth'
    String album = 'The Crusher'
    String url = 'http://amonamarth.com'
    List<String> tags = ['Death Metal', 'Viking Metal', 'Melodic Death Metal']
    List<String> genres = ['Doom Metal', 'Black Metal']
    List<String> relatedBands = ['Ensiferum', 'Bathory']
    String score = '5.0'
    boolean processed = false
    String imageUrl = 'http://amonamarth.com/logo.jpg'
    LocalDateTime createdDate = LocalDateTime.now()
    boolean created = false

    AlbumReviewBuilder withId(String id) {
        this.id = id
        return this
    }

    AlbumReviewBuilder withBand(String band) {
        this.band = band
        return this
    }

    AlbumReviewBuilder withAlbum(String album) {
        this.album = album
        return this
    }

    AlbumReviewBuilder withUrl(String url) {
        this.url = url
        return this
    }

    AlbumReviewBuilder withTags(List<String> tags) {
        this.tags = tags
        return this
    }

    AlbumReviewBuilder withGenres(List<String> genres) {
        this.genres = genres
        return this
    }

    AlbumReviewBuilder withRelatedBands(List<String> relatedBands) {
        this.relatedBands = relatedBands
        return this
    }

    AlbumReviewBuilder withScore(String score) {
        this.score = score
        return this
    }

    AlbumReviewBuilder withProcessed(boolean processed) {
        this.processed = processed
        return this
    }

    AlbumReviewBuilder withImageUrl(String imageUrl) {
        this.imageUrl = imageUrl
        return this
    }

    AlbumReviewBuilder withCreatedDate(LocalDateTime createdDate) {
        this.createdDate = createdDate
        return this
    }

    AlbumReviewBuilder withCreated(boolean created) {
        this.created = created
        return this
    }

    AlbumReviewBuilder fromReviewEntity(ReviewEntity reviewEntity) {
        this.id = reviewEntity.getId()
        this.band = reviewEntity.getBand()
        this.album = reviewEntity.getAlbum()
        this.url = reviewEntity.getUrl()
        this.tags = reviewEntity.getTags().collect()
        this.genres = reviewEntity.getGenres().collect()
        this.relatedBands = reviewEntity.getRelatedBands().collect()
        this.score = reviewEntity.getScore()
        this.processed = reviewEntity.isProcessed()
        this.createdDate = reviewEntity.getCreatedDate()
        this.imageUrl = reviewEntity.getImageUrl()
        return this
    }

    AlbumReview build() {
        return new AlbumReview(
            id: id,
            band: band,
            album: album,
            url: url,
            tags: tags,
            genres: genres,
            relatedBands: relatedBands,
            score: score,
            processed: processed,
            imageUrl: imageUrl,
            createdDate: createdDate,
            created: created
        )
    }

}
