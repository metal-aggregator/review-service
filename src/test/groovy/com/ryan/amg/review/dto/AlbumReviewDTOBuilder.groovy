package com.ryan.amg.review.dto

import com.ryan.amg.review.domain.AlbumReview

import java.time.LocalDateTime

class AlbumReviewDTOBuilder {
    private String id = '1234-5678'
    private String band = 'Funeral Leech'
    private String album = 'Death Meditation'
    private String url = 'https://www.angrymetalguy.com/funeral-leech-death-meditation-review/'
    private List<String> tags = ['Death Metal', 'American Metal']
    private List<String> genres = ['Doom Metal', 'Black Metal']
    private List<String> relatedBands = ['Ensiferum', 'Bathory']
    private String score = '5.0'
    private boolean processed = false
    private LocalDateTime createdDate = LocalDateTime.now()
    private String imageUrl = 'http://image.com'

    AlbumReviewDTOBuilder fromAlbumReview(AlbumReview albumReview) {
        this.id = albumReview.id
        this.band = albumReview.band
        this.album = albumReview.album
        this.url = albumReview.url
        this.tags = albumReview.tags.collect()
        this.genres = albumReview.genres.collect()
        this.relatedBands = albumReview.relatedBands.collect()
        this.score = albumReview.score
        this.processed = albumReview.processed
        this.createdDate = albumReview.createdDate
        this.imageUrl = albumReview.imageUrl
        return this
    }

    AlbumReviewDTO build() {
        return new AlbumReviewDTO(
            id: id,
            band: band,
            album: album,
            url: url,
            tags: tags,
            genres: genres,
            relatedBands: relatedBands,
            score: score,
            processed: processed,
            createdDate: createdDate,
            imageUrl: imageUrl
        )
    }
}
