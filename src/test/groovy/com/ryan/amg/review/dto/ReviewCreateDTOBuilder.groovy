package com.ryan.amg.review.dto

import com.ryan.amg.review.domain.AlbumReview

class ReviewCreateDTOBuilder {
    private String band = 'Funeral Leech'
    private String album = 'Death Meditation'
    private String url = 'https://www.angrymetalguy.com/funeral-leech-death-meditation-review/'
    private List<String> tags = ['Death Metal', 'American Metal']
    private List<String> genres = ['Crust Punk', 'Sludge']
    private List<String> relatedBands = ['Darkthrone', 'Algebra']
    private String score = '5.0'
    private String imageUrl = 'http://image.com'

    ReviewCreateDTOBuilder fromAlbumReview(AlbumReview albumReview) {
        this.band = albumReview.band
        this.album = albumReview.album
        this.url = albumReview.url
        this.tags = albumReview.tags.collect()
        this.genres = albumReview.genres.collect()
        this.relatedBands = albumReview.relatedBands.collect()
        this.score = albumReview.score
        this.imageUrl = albumReview.imageUrl
        return this
    }

    ReviewCreateDTO build() {
        return new ReviewCreateDTO(
            band: band,
            album: album,
            url: url,
            tags: tags,
            genres: genres,
            relatedBands: relatedBands,
            score: score,
            imageUrl: imageUrl
        )
    }
}
