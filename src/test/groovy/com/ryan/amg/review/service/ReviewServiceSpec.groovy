package com.ryan.amg.review.service

import com.ryan.amg.review.db.repository.ReviewRepository
import com.ryan.amg.review.db.entity.ReviewEntityBuilder
import com.ryan.amg.review.domain.AlbumReviewBuilder
import com.ryan.amg.review.domain.AlbumReview
import com.ryan.amg.review.db.entity.ReviewEntity
import com.ryan.amg.review.exception.ReviewNotFoundException
import org.junit.Assert
import org.mockito.internal.matchers.apachecommons.ReflectionEquals
import org.unitils.reflectionassert.ReflectionComparatorMode
import spock.lang.Specification

import java.time.Duration

import static org.junit.Assert.assertTrue
import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals

class ReviewServiceSpec extends Specification {

    ReviewRepository mockReviewRepository = Mock()

    ReviewService reviewService = new ReviewService(mockReviewRepository)

    def "Calling createReview invokes the repository and returns the expected domain object when the entity does not already exist"() {

        given:
            AlbumReview inputAlbumReview = new AlbumReviewBuilder().withId(null).build()

            ReviewEntity expectedReviewEntity = new ReviewEntityBuilder().fromAlbumReview(new AlbumReviewBuilder().withId(null).build()).withId(null).build()
            ReviewEntity mockedReviewEntity = new ReviewEntityBuilder().build()

            AlbumReview expectedAlbumReview = new AlbumReviewBuilder().withCreated(true).build()

            1 * mockReviewRepository.findByUrl(inputAlbumReview.getUrl()) >> null

            1 * mockReviewRepository.save(_ as ReviewEntity) >> { args ->
                ReviewEntity actualReviewEntity = (ReviewEntity) args.get(0)
                assertTrue(new ReflectionEquals(expectedReviewEntity, "createdDate").matches(actualReviewEntity))
                assertTrue(1 >= Duration.between(expectedReviewEntity.createdDate, actualReviewEntity.createdDate).toSeconds())
                return mockedReviewEntity
            }

        when:
            AlbumReview actualAlbumReview = reviewService.createAlbumReview(inputAlbumReview)

        then:
            assertTrue(new ReflectionEquals(expectedAlbumReview, "createdDate").matches(actualAlbumReview))
            assertTrue(1 >= Duration.between(expectedAlbumReview.createdDate, actualAlbumReview.createdDate).toSeconds())

    }

    def "Calling createReview invokes the repository and returns the expected domain object when the entity already exists"() {

        given:
            AlbumReview inputAlbumReview = new AlbumReviewBuilder().withId(null).build()

            ReviewEntity mockedReviewEntity = new ReviewEntityBuilder().build()

            AlbumReview expectedAlbumReview = new AlbumReviewBuilder().withCreated(false).build()

            1 * mockReviewRepository.findByUrl(inputAlbumReview.getUrl()) >> [mockedReviewEntity]
            0 * mockReviewRepository.save(_ as ReviewEntity)

        when:
            AlbumReview actualAlbumReview = reviewService.createAlbumReview(inputAlbumReview)

        then:
            assertTrue(new ReflectionEquals(expectedAlbumReview, "createdDate").matches(actualAlbumReview))
            assertTrue(1 >= Duration.between(expectedAlbumReview.createdDate, actualAlbumReview.createdDate).toSeconds())

    }

    def "Calling addGenreToReview invokes the repository and correctly adds the specified genre"() {

        given:
            String inputReviewId = 'ReviewId-123'
            String inputGenre = 'Z'

            ReviewEntity mockReadReviewEntity = new ReviewEntityBuilder().withGenres(['X', 'Y']).build()

            ReviewEntity expectedWriteReviewEntity = new ReviewEntityBuilder().withGenres(['X', 'Y', 'Z']).build()
            ReviewEntity mockedWriteReviewEntity = new ReviewEntityBuilder().withGenres(['X', 'Y', 'Z']).build()

            AlbumReview expectedAlbumReview = new AlbumReviewBuilder().fromReviewEntity(new ReviewEntityBuilder().withGenres(['X', 'Y', 'Z']).build()).build()

            1 * mockReviewRepository.findById('ReviewId-123') >> Optional.of(mockReadReviewEntity)
            1 * mockReviewRepository.save(_ as ReviewEntity) >> { args ->
                ReviewEntity actualWriteReviewEntity = (ReviewEntity) args.get(0)
                assertTrue(new ReflectionEquals(expectedWriteReviewEntity, "createdDate").matches(actualWriteReviewEntity))
                assertTrue(1 >= Duration.between(expectedWriteReviewEntity.createdDate, actualWriteReviewEntity.createdDate).toSeconds())
                return mockedWriteReviewEntity
            }

        when:
            AlbumReview actualAlbumReview = reviewService.addGenreToReview(inputReviewId, inputGenre)

        then:
            assertTrue(new ReflectionEquals(expectedAlbumReview, "createdDate").matches(actualAlbumReview))
            assertTrue(1 >= Duration.between(expectedAlbumReview.createdDate, actualAlbumReview.createdDate).toSeconds())

    }

    def "Calling addGenreToReview throws the expected ReviewNotFoundException when the specified review id is not found in the repository"() {

        given:
            1 * mockReviewRepository.findById('ReviewId') >> Optional.ofNullable(null)

        when:
            reviewService.addGenreToReview('ReviewId', 'SomeGenre')

        then:
            thrown(ReviewNotFoundException)

    }

    def "Calling addRelatedBandToReview invokes the repository and correctly adds the specified related band"() {

        given:
            String inputReviewId = 'ReviewId-123'
            String inputRelatedBand = 'Z'

            ReviewEntity mockReadReviewEntity = new ReviewEntityBuilder().withRelatedBands(['X', 'Y']).build()

            ReviewEntity expectedWriteReviewEntity = new ReviewEntityBuilder().withRelatedBands(['X', 'Y', 'Z']).build()
            ReviewEntity mockedWriteReviewEntity = new ReviewEntityBuilder().withRelatedBands(['X', 'Y', 'Z']).build()

            AlbumReview expectedAlbumReview = new AlbumReviewBuilder().fromReviewEntity(new ReviewEntityBuilder().withRelatedBands(['X', 'Y', 'Z']).build()).build()

            1 * mockReviewRepository.findById('ReviewId-123') >> Optional.of(mockReadReviewEntity)
            1 * mockReviewRepository.save(_ as ReviewEntity) >> { args ->
                ReviewEntity actualWriteReviewEntity = (ReviewEntity) args.get(0)
                assertTrue(new ReflectionEquals(expectedWriteReviewEntity, "createdDate").matches(actualWriteReviewEntity))
                assertTrue(1 >= Duration.between(expectedWriteReviewEntity.createdDate, actualWriteReviewEntity.createdDate).toSeconds())
                return mockedWriteReviewEntity
            }

        when:
            AlbumReview actualAlbumReview = reviewService.addRelatedBandToReview(inputReviewId, inputRelatedBand)

        then:
            assertTrue(new ReflectionEquals(expectedAlbumReview, "createdDate").matches(actualAlbumReview))
            assertTrue(1 >= Duration.between(expectedAlbumReview.createdDate, actualAlbumReview.createdDate).toSeconds())

    }

    def "Calling addRelatedBandToReview throws the expected ReviewNotFoundException when the specified review id is not found in the repository"() {

        given:
            1 * mockReviewRepository.findById('ReviewId') >> Optional.ofNullable(null)

        when:
            reviewService.addRelatedBandToReview('ReviewId', 'SomeBand')

        then:
            thrown(ReviewNotFoundException)

    }

    def "Calling addScoreToReview invokes the repository and correctly sets the score"() {

        given:
            String inputReviewId = 'ReviewId-123'
            String inputScore = '4.5'

            ReviewEntity mockReadReviewEntity = new ReviewEntityBuilder().withScore(null).build()

            ReviewEntity expectedWriteReviewEntity = new ReviewEntityBuilder().withScore('4.5').build()
            ReviewEntity mockedWriteReviewEntity = new ReviewEntityBuilder().withScore('4.5').build()

            AlbumReview expectedAlbumReview = new AlbumReviewBuilder().fromReviewEntity(new ReviewEntityBuilder().withScore('4.5').build()).build()

            1 * mockReviewRepository.findById('ReviewId-123') >> Optional.of(mockReadReviewEntity)
            1 * mockReviewRepository.save(_ as ReviewEntity) >> { args ->
                ReviewEntity actualWriteReviewEntity = (ReviewEntity) args.get(0)
                assertTrue(new ReflectionEquals(expectedWriteReviewEntity, "createdDate").matches(actualWriteReviewEntity))
                assertTrue(1 >= Duration.between(expectedWriteReviewEntity.createdDate, actualWriteReviewEntity.createdDate).toSeconds())
                return mockedWriteReviewEntity
            }

        when:
            AlbumReview actualAlbumReview = reviewService.addScoreToReview(inputReviewId, inputScore)

        then:
            assertTrue(new ReflectionEquals(expectedAlbumReview, "createdDate").matches(actualAlbumReview))
            assertTrue(1 >= Duration.between(expectedAlbumReview.createdDate, actualAlbumReview.createdDate).toSeconds())

    }

    def "Calling addScoreToReview throws the expected ReviewNotFoundException when the specified review id is not found in the repository"() {

        given:
            1 * mockReviewRepository.findById('ReviewId') >> Optional.ofNullable(null)

        when:
            reviewService.addScoreToReview('ReviewId', 'SomeScore')

        then:
            thrown(ReviewNotFoundException)

    }

    def "Calling toggleReviewProcessed invokes the repository and correctly sets the processed flag"() {

        given:
            String inputReviewId = 'ReviewId-123'
            boolean processed = true

            ReviewEntity mockReadReviewEntity = new ReviewEntityBuilder().withProcessed(false).build()

            ReviewEntity expectedWriteReviewEntity = new ReviewEntityBuilder().withProcessed(true).build()
            ReviewEntity mockedWriteReviewEntity = new ReviewEntityBuilder().withProcessed(true).build()

            AlbumReview expectedAlbumReview = new AlbumReviewBuilder().fromReviewEntity(new ReviewEntityBuilder().withProcessed(true).build()).build()

            1 * mockReviewRepository.findById('ReviewId-123') >> Optional.of(mockReadReviewEntity)
            1 * mockReviewRepository.save(_ as ReviewEntity) >> { args ->
                ReviewEntity actualWriteReviewEntity = (ReviewEntity) args.get(0)
                assertTrue(new ReflectionEquals(expectedWriteReviewEntity, "createdDate").matches(actualWriteReviewEntity))
                assertTrue(1 >= Duration.between(expectedWriteReviewEntity.createdDate, actualWriteReviewEntity.createdDate).toSeconds())
                return mockedWriteReviewEntity
            }

        when:
            AlbumReview actualAlbumReview = reviewService.toggleReviewAsProcessed(inputReviewId, processed)

        then:
            assertTrue(new ReflectionEquals(expectedAlbumReview, "createdDate").matches(actualAlbumReview))
            assertTrue(1 >= Duration.between(expectedAlbumReview.createdDate, actualAlbumReview.createdDate).toSeconds())

    }

    def "Calling toggleReviewProcessed throws the expected ReviewNotFoundException when the specified review id is not found in the repository"() {

        given:
            1 * mockReviewRepository.findById('ReviewId') >> Optional.ofNullable(null)

        when:
            reviewService.toggleReviewAsProcessed('ReviewId', true)

        then:
            thrown(ReviewNotFoundException)

    }

    def "Calling deleteAlbumReview invokes the repository with the correct review id"() {

        given:
            String inputReviewId = 'id-123'
            1 * mockReviewRepository.deleteById(inputReviewId)

        when:
            reviewService.deleteAlbumReview(inputReviewId)

        then:
            noExceptionThrown()

    }

}
