package com.ryan.amg.review.controller

import com.ryan.amg.review.domain.AlbumReview
import com.ryan.amg.review.domain.AlbumReviewBuilder
import com.ryan.amg.review.dto.AlbumReviewDTO
import com.ryan.amg.review.dto.AlbumReviewDTOBuilder
import com.ryan.amg.review.service.ReviewService
import org.mockito.internal.matchers.apachecommons.ReflectionEquals
import spock.lang.Specification

import java.time.Duration

import static org.junit.Assert.assertTrue
import static org.junit.Assert.assertTrue
import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals

class ProcessedControllerSpec extends Specification {

    ReviewService mockReviewService = Mock()

    ProcessedController processedController = new ProcessedController(mockReviewService)

    def "Calling toggleReviewAsProcessed correctly invokes the service layer and returns the expected DTO"() {

        given:
            AlbumReview mockedAlbumReview = new AlbumReviewBuilder().withProcessed(true).build()
            AlbumReviewDTO expectedOutputAlbumReview = new AlbumReviewDTOBuilder().fromAlbumReview(new AlbumReviewBuilder().withProcessed(true).build()).build()

            1 * mockReviewService.toggleReviewAsProcessed('ReviewId', true) >> mockedAlbumReview

        when:
            AlbumReviewDTO actualOutputAlbumReview = processedController.toggleReviewAsProcessed('ReviewId', true)

        then:
            assertTrue(new ReflectionEquals(expectedOutputAlbumReview, "createdDate").matches(actualOutputAlbumReview))

    }

}
