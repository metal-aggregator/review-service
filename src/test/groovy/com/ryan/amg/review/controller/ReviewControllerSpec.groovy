package com.ryan.amg.review.controller

import com.ryan.amg.review.domain.AlbumReview
import com.ryan.amg.review.domain.AlbumReviewBuilder
import com.ryan.amg.review.dto.AlbumReviewDTO
import com.ryan.amg.review.dto.AlbumReviewDTOBuilder
import com.ryan.amg.review.dto.ReviewCreateDTO
import com.ryan.amg.review.dto.ReviewCreateDTOBuilder
import com.ryan.amg.review.service.ReviewService
import org.mockito.internal.matchers.apachecommons.ReflectionEquals
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.unitils.reflectionassert.ReflectionComparatorMode
import spock.lang.Specification
import spock.lang.Unroll

import java.time.Duration

import static org.junit.Assert.assertTrue
import static org.junit.Assert.assertTrue
import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals

class ReviewControllerSpec extends Specification {

    ReviewService mockReviewService = Mock()

    ReviewController reviewController = new ReviewController(mockReviewService)


    def "Invoking createReview calls the service layer and returns the expected domain object where createdFlag=#description"() {

        given:
            AlbumReview mockedAlbumReview = new AlbumReviewBuilder().withCreated(createdFlag).build()
            AlbumReviewDTO expectedOutputAlbumReview = new AlbumReviewDTOBuilder().fromAlbumReview(new AlbumReviewBuilder().withCreated(createdFlag).build()).build()

            ReviewCreateDTO inputReviewCreateDTO = new ReviewCreateDTOBuilder().fromAlbumReview(new AlbumReviewBuilder().withCreated(createdFlag).build()).build()
            AlbumReview expectedServiceAlbumReview = new AlbumReviewBuilder().withId(null).withCreatedDate(null).build()

            1 * mockReviewService.createAlbumReview(_ as AlbumReview) >> { args ->
                assertReflectionEquals(expectedServiceAlbumReview, (AlbumReview) args.get(0), ReflectionComparatorMode.LENIENT_ORDER)
                return mockedAlbumReview
            }

        when:
            ResponseEntity<AlbumReviewDTO> actualAlbumReviewEntity = reviewController.createReview(inputReviewCreateDTO)

        then:
            expectedStatus == actualAlbumReviewEntity.getStatusCode()
            assertTrue(new ReflectionEquals(expectedOutputAlbumReview, "createdDate").matches(actualAlbumReviewEntity.getBody()))
            assertTrue(1 >= Duration.between(expectedOutputAlbumReview.createdDate, actualAlbumReviewEntity.getBody().createdDate).toSeconds())

        where:
            description   | createdFlag | expectedStatus
            'created'     | true        | HttpStatus.CREATED
            'not created' | false       | HttpStatus.OK

    }

    def "Invoking deleteReview calls the service layer with the expected id"() {

        given:
            String inputReviewId = 'id-123'
            1 * mockReviewService.deleteAlbumReview(inputReviewId)

        when:
            reviewController.deleteReview(inputReviewId)

        then:
            noExceptionThrown()

    }

}
