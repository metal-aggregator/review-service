package com.ryan.amg.review.controller

import com.ryan.amg.review.domain.AlbumReview
import com.ryan.amg.review.domain.AlbumReviewBuilder
import com.ryan.amg.review.dto.AlbumReviewDTO
import com.ryan.amg.review.dto.AlbumReviewDTOBuilder
import com.ryan.amg.review.service.ReviewService
import org.mockito.internal.matchers.apachecommons.ReflectionEquals
import spock.lang.Specification

import java.time.Duration

import static org.junit.Assert.assertTrue
import static org.junit.Assert.assertTrue
import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals

class ScoreControllerSpec extends Specification {

    ReviewService mockReviewService = Mock()

    ScoreController scoreController = new ScoreController(mockReviewService)

    def "Calling addScoreToReview correctly invokes the service layer and returns the expected DTO"() {

        given:
            AlbumReview mockedAlbumReview = new AlbumReviewBuilder().build()
            AlbumReviewDTO expectedOutputAlbumReview = new AlbumReviewDTOBuilder().fromAlbumReview(new AlbumReviewBuilder().build()).build()

            1 * mockReviewService.addScoreToReview('ReviewId', '4.0') >> mockedAlbumReview

        when:
            AlbumReviewDTO actualOutputAlbumReview = scoreController.addScoreToReview('ReviewId', '4.0')

        then:
            assertTrue(new ReflectionEquals(expectedOutputAlbumReview, "createdDate").matches(actualOutputAlbumReview))
            assertTrue(1 >= Duration.between(expectedOutputAlbumReview.createdDate, actualOutputAlbumReview.createdDate).toSeconds())

    }

}
