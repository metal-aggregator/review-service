package com.ryan.amg.review.exception

import spock.lang.Specification

class ReviewNotFoundExceptionSpec extends Specification {

    def "Creating a new ReviewNotFoundException correctly formats the exception message with the input reviewId"() {

        when:
            ReviewNotFoundException actualException = new ReviewNotFoundException('ReviewId')

        then:
            actualException.message == 'The review with reviewId=ReviewId was not found.'

    }

}
