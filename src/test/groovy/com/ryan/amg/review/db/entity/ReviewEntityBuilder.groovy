package com.ryan.amg.review.db.entity

import com.ryan.amg.review.domain.AlbumReview

import java.time.LocalDateTime

class ReviewEntityBuilder {
    String id = 'id-12345'
    String band = 'Amon Amarth'
    String album = 'The Crusher'
    String url = 'http://amonamarth.com'
    List<String> tags = ['Death Metal', 'Viking Metal', 'Melodic Death Metal']
    List<String> genres = ['Doom Metal', 'Black Metal']
    List<String> relatedBands = ['Ensiferum', 'Bathory']
    String score = '5.0'
    boolean processed = false
    LocalDateTime createdDate = LocalDateTime.now()
    String imageUrl = 'http://amonamarth.com/logo.jpg'

    ReviewEntityBuilder fromAlbumReview(AlbumReview albumReview) {
        this.id = albumReview.id
        this.band = albumReview.band
        this.album = albumReview.album
        this.url = albumReview.url
        this.tags = albumReview.tags.collect()
        this.genres = albumReview.genres.collect()
        this.relatedBands = albumReview.relatedBands.collect()
        this.score = albumReview.score
        this.processed = albumReview.processed
        this.imageUrl = albumReview.imageUrl
        return this
    }

    ReviewEntityBuilder withId(String id) {
        this.id = id
        return this
    }

    ReviewEntityBuilder withBand(String band) {
        this.band = band
        return this
    }

    ReviewEntityBuilder withAlbum(String album) {
        this.album = album
        return this
    }

    ReviewEntityBuilder withUrl(String url) {
        this.url = url
        return this
    }

    ReviewEntityBuilder withTags(List<String> tags) {
        this.tags = tags
        return this
    }

    ReviewEntityBuilder withGenres(List<String> genres) {
        this.genres = genres
        return this
    }

    ReviewEntityBuilder withRelatedBands(List<String> relatedBands) {
        this.relatedBands = relatedBands
        return this
    }

    ReviewEntityBuilder withScore(String score) {
        this.score = score
        return this
    }

    ReviewEntityBuilder withProcessed(boolean processed) {
        this.processed = processed
        return this
    }

    ReviewEntityBuilder withImageUrl(String imageUrl) {
        this.imageUrl = imageUrl
        return this
    }

    ReviewEntity build() {
        return new ReviewEntity(
            id: id,
            band: band,
            album: album,
            url: url,
            tags: tags,
            genres: genres,
            relatedBands: relatedBands,
            score: score,
            processed: processed,
            createdDate: createdDate,
            imageUrl: imageUrl
        )
    }
    
}
