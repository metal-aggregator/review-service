package com.ryan.amg.review.db.entity

import spock.lang.Specification

class ReviewEntitySpec extends Specification {

    def "Calling addGenre adds the specified genre to the genre list with no duplicates"() {

        given:
            ReviewEntity reviewEntity = new ReviewEntityBuilder().withGenres([]).build()

        when:
            reviewEntity.addGenre('Death Metal')

        then:
            reviewEntity.genres == ['Death Metal']

        when:
            reviewEntity.addGenre('Black Metal')

        then:
            reviewEntity.genres == ['Death Metal', 'Black Metal']

        when:
            reviewEntity.addGenre('Black Metal')

        then:
            reviewEntity.genres == ['Death Metal', 'Black Metal']

    }

    def "Calling addRelatedBand adds the specified band to the related band list with no duplicates"() {

        given:
            ReviewEntity reviewEntity = new ReviewEntityBuilder().withRelatedBands([]).build()

        when:
            reviewEntity.addRelatedBand('Ensiferum')

        then:
            reviewEntity.relatedBands == ['Ensiferum']

        when:
            reviewEntity.addRelatedBand('Bathory')

        then:
            reviewEntity.relatedBands == ['Ensiferum', 'Bathory']

        when:
            reviewEntity.addRelatedBand('Ensiferum')

        then:
            reviewEntity.relatedBands == ['Ensiferum', 'Bathory']

    }

}
