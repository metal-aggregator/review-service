package com.ryan.amg.review.config

import groovyx.net.http.RESTClient

class RestClientBuilder {

    static RESTClient build(String targetHost) {
        RESTClient restClient = new RESTClient(targetHost)
        restClient.handler.failure = { response, data ->
            response.setData(data)
            return response
        }
        return restClient
    }

}
