package com.ryan.amg.review.config

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Configuration

@Configuration
@ConfigurationProperties(prefix = "acceptance")
class AcceptanceTestConfig {

    TargetProperties target

    static class TargetProperties {
        String env
        String host

        void setEnv(String env) {
            this.env = env
        }

        void setHost(String host) {
            this.host = host
        }

    }

    void setTarget(TargetProperties target) {
        this.target = target
    }
}
