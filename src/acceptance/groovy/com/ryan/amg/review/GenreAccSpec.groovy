package com.ryan.amg.review

import org.springframework.http.MediaType

class GenreAccSpec extends AcceptanceTest {

    def "PUT /api/review/{reviewId}/genres/{genre} Adds a genre to an existing review"() {

        given:
            String uuid = UUID.randomUUID().toString()
            def reviewRequest = [
                band: 'TestBand_' + uuid,
                album: 'TestAlbum_' + uuid,
                url: 'http://band_' + uuid + '.com',
                tags: ['Genre_' + uuid, 'Score_' + uuid],
                imageUrl: 'http://imageurl_' + uuid + '.com'
            ]

        when: 'We create a review'
            def postResponse = restClient.post(
                path: "/api/reviews",
                headers: [
                    'Accept': MediaType.APPLICATION_JSON_VALUE
                ],
                contentType: MediaType.APPLICATION_JSON_VALUE,
                body: reviewRequest
            )
            def actualPostResponse = postResponse.data

        then: 'Verify that the review was created'
            postResponse.status == 201

        when: 'We add one genre to the review'
            def putResponse = restClient.put(path: "/api/reviews/${actualPostResponse.id}/genres/Death Metal")
            def actualPutResponse = putResponse.data

        then: 'Verify that the genre was added'
            actualPutResponse.genres == ['Death Metal']

        when: 'We add a second genre to the review'
            putResponse = restClient.put(path: "/api/reviews/${actualPostResponse.id}/genres/Black Metal")
            actualPutResponse = putResponse.data

        then: 'Verify that the genre was added alongside the first'
            actualPutResponse.genres.size() == 2
            actualPutResponse.genres.containsAll(['Black Metal', 'Death Metal'])

        when: 'We try to add a duplicate genre to the review'
            putResponse = restClient.put(path: "/api/reviews/${actualPostResponse.id}/genres/Black Metal")
            actualPutResponse = putResponse.data

        then: 'Verify that the duplicate genre was not added'
            actualPutResponse.genres.size() == 2
            actualPutResponse.genres.containsAll(['Black Metal', 'Death Metal'])

        cleanup:
            restClient.delete(path: "/api/reviews/${actualPostResponse.id}")

    }

    def "PUT /api/review/{reviewId}/genres/{genre} Returns a 404 when the review does not exist"() {

        when:
            def putResponse = restClient.put(path: "/api/reviews/NonExistent/genres/Death Metal")
            def putResponseBody = putResponse.data

        then:
            putResponse.status == 404
            putResponseBody.message == 'The review with reviewId=NonExistent was not found.'

    }

}
