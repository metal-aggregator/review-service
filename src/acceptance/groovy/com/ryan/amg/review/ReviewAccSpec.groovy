package com.ryan.amg.review

import org.springframework.http.MediaType

class ReviewAccSpec extends AcceptanceTest {

    def "POST /api/review Creates a new review and receives a 201 when the review doesn't already exist"() {

        given:
            String uuid = UUID.randomUUID().toString()
            def reviewRequest = [
                band: 'TestBand_' + uuid,
                album: 'TestAlbum_' + uuid,
                url: 'http://band_' + uuid + '.com',
                tags: ['Genre_' + uuid, 'Score_' + uuid],
                imageUrl: 'http://imageurl_' + uuid + '.com'
            ]

        when:
            def response = restClient.post(
                path: "/api/reviews",
                headers: [
                    'Accept': MediaType.APPLICATION_JSON_VALUE
                ],
                contentType: MediaType.APPLICATION_JSON_VALUE,
                body: reviewRequest
            )
            def actualResponse = response.data

        then:
            response.status == 201
            actualResponse.id
            actualResponse.band == reviewRequest.band
            actualResponse.album == reviewRequest.album
            actualResponse.url == reviewRequest.url
            actualResponse.tags.each { assert reviewRequest.tags.contains(it) }
            actualResponse.imageUrl == reviewRequest.imageUrl
            actualResponse.createdDate

        cleanup:
            restClient.delete(path: "/api/reviews/${actualResponse.id}")

    }

    def "POST /api/review Creates a new review and receives a 200 when the review with the same url already exists"() {

        given:
            String uuid = UUID.randomUUID().toString()
            def reviewRequest = [
                band: 'TestBand_' + uuid,
                album: 'TestAlbum_' + uuid,
                url: 'http://band_' + uuid + '.com',
                tags: ['Genre_' + uuid, 'Score_' + uuid],
                imageUrl: 'http://imageurl_' + uuid + '.com'
            ]

        when:
            def response = restClient.post(
                path: "/api/reviews",
                headers: [
                    'Accept': MediaType.APPLICATION_JSON_VALUE
                ],
                contentType: MediaType.APPLICATION_JSON_VALUE,
                body: reviewRequest
            )
            def firstResponse = response.data

        then:
            response.status == 201

        when:
            response = restClient.post(
                path: "/api/reviews",
                headers: [
                    'Accept': MediaType.APPLICATION_JSON_VALUE
                ],
                contentType: MediaType.APPLICATION_JSON_VALUE,
                body: reviewRequest
            )
            def secondResponse = response.data

        then:
            response.status == 200
            secondResponse.id == firstResponse.id
            secondResponse.band == firstResponse.band
            secondResponse.album == firstResponse.album
            secondResponse.url == firstResponse.url
            secondResponse.tags == firstResponse.tags
            secondResponse.imageUrl == firstResponse.imageUrl
            secondResponse.createdDate == firstResponse.createdDate

        cleanup:
            restClient.delete(path: "/api/reviews/${firstResponse.id}")

    }

    def "DELETE /api/reviews/{reviewId} Deletes a review when an existing review id is provided"() {

        given:
            String uuid = UUID.randomUUID().toString()
            def reviewRequest = [
                band: 'TestBand_' + uuid,
                album: 'TestAlbum_' + uuid,
                url: 'http://band_' + uuid + '.com',
                tags: ['Genre_' + uuid, 'Score_' + uuid],
                imageUrl: 'http://imageurl_' + uuid + '.com'
            ]

        when:
            def response = restClient.post(
                path: "/api/reviews",
                headers: [
                    'Accept': MediaType.APPLICATION_JSON_VALUE
                ],
                contentType: MediaType.APPLICATION_JSON_VALUE,
                body: reviewRequest
            )

        then:
            response.status == 201

        when:
            def reviewDeletionResponse = restClient.delete(
                path: "/api/reviews/${response.data.id}"
            )

        then:
            reviewDeletionResponse.status == 204

    }

}
