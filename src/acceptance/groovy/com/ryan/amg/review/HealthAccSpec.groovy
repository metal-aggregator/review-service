package com.ryan.amg.review

import org.springframework.http.MediaType

class HealthAccSpec extends AcceptanceTest {

    def "GET /actuator/health Asserts that the microservice is healthy"() {

        when:
            def response = restClient.get(
                path: "/",
                headers: [
                    'Accept': MediaType.APPLICATION_JSON_VALUE
                ]
            )
            Map health = response.data

        then:
            response.status == 200
            health.status == 'UP'

    }

}
