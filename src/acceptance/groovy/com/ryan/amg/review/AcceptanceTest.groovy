package com.ryan.amg.review

import com.ryan.amg.review.config.AcceptanceTestConfig
import com.ryan.amg.review.config.RestClientBuilder
import groovyx.net.http.RESTClient
import org.apache.commons.logging.Log
import org.apache.commons.logging.LogFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.test.context.ConfigFileApplicationContextInitializer
import org.springframework.context.annotation.Configuration
import org.springframework.test.context.ContextConfiguration
import spock.lang.Shared
import spock.lang.Specification

@Configuration
@ContextConfiguration(initializers = [ConfigFileApplicationContextInitializer])
@EnableConfigurationProperties(AcceptanceTestConfig)
abstract class AcceptanceTest extends Specification {

    protected static final Log LOG = LogFactory.getLog(getClass())

    @Shared RESTClient restClient
    @Shared boolean initialized = false

    @Autowired AcceptanceTestConfig acceptanceTestConfig

    def initializeTests() {
        if (!initialized) {
            restClient = RestClientBuilder.build(acceptanceTestConfig.target.host)
            initialized = true
        }
    }

    def setup() {
        initializeTests()

        LOG.info('===========================================================================================================================================================')
        LOG.info("Initiating test: ${specificationContext.currentIteration.name}")
        LOG.info('===========================================================================================================================================================')
    }

    def cleanup() {
        LOG.info('===========================================================================================================================================================')
        LOG.info("Completing test: ${specificationContext.currentIteration.name}")
        LOG.info('===========================================================================================================================================================')
    }

}
