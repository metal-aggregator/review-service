package com.ryan.amg.review

import org.springframework.http.MediaType

class RelatedBandAccSpec extends AcceptanceTest {

    def "PUT /api/review/{reviewId}/relatedBands/{relatedBand} Adds a related band to an existing review"() {

        given:
            String uuid = UUID.randomUUID().toString()
            def reviewRequest = [
                band: 'TestBand_' + uuid,
                album: 'TestAlbum_' + uuid,
                url: 'http://band_' + uuid + '.com',
                tags: ['Genre_' + uuid, 'Score_' + uuid],
                imageUrl: 'http://imageurl_' + uuid + '.com'
            ]

        when: 'We create a review'
            def postResponse = restClient.post(
                path: "/api/reviews",
                headers: [
                    'Accept': MediaType.APPLICATION_JSON_VALUE
                ],
                contentType: MediaType.APPLICATION_JSON_VALUE,
                body: reviewRequest
            )
            def actualPostResponse = postResponse.data

        then: 'Verify that the review was created'
            postResponse.status == 201

        when: 'We add one related band to the review'
            def putResponse = restClient.put(path: "/api/reviews/${actualPostResponse.id}/relatedBands/Anthrax")
            def actualPutResponse = putResponse.data

        then: 'Verify that the related band was added'
            actualPutResponse.relatedBands == ['Anthrax']

        when: 'We add a second related band to the review'
            putResponse = restClient.put(path: "/api/reviews/${actualPostResponse.id}/relatedBands/Testament")
            actualPutResponse = putResponse.data

        then: 'Verify that the related band was added alongside the first'
            actualPutResponse.relatedBands.size() == 2
            actualPutResponse.relatedBands.containsAll(['Anthrax', 'Testament'])

        when: 'We try to add a duplicate related band to the review'
            putResponse = restClient.put(path: "/api/reviews/${actualPostResponse.id}/relatedBands/Anthrax")
            actualPutResponse = putResponse.data

        then: 'Verify that the duplicate related band was not added'
            actualPutResponse.relatedBands.size() == 2
            actualPutResponse.relatedBands.containsAll(['Anthrax', 'Testament'])

        cleanup:
            restClient.delete(path: "/api/reviews/${actualPostResponse.id}")

    }

    def "PUT /api/review/{reviewId}/genres/{genre} Returns a 404 when the review does not exist"() {

        when:
            def putResponse = restClient.put(path: "/api/reviews/NonExistent/relatedBands/Anthrax")
            def putResponseBody = putResponse.data

        then:
            putResponse.status == 404
            putResponseBody.message == 'The review with reviewId=NonExistent was not found.'

    }

}
