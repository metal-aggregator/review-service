package com.ryan.amg.review

import org.springframework.http.MediaType

class ScoreAccSpec extends AcceptanceTest {

    def "PUT /api/review/{reviewId}/score/{score} Adds the score to an existing review"() {

        given:
            String uuid = UUID.randomUUID().toString()
            def reviewRequest = [
                band: 'TestBand_' + uuid,
                album: 'TestAlbum_' + uuid,
                url: 'http://band_' + uuid + '.com',
                tags: ['Genre_' + uuid, 'Score_' + uuid],
                imageUrl: 'http://imageurl_' + uuid + '.com'
            ]

        when: 'We create a review'
            def postResponse = restClient.post(
                path: "/api/reviews",
                headers: [
                    'Accept': MediaType.APPLICATION_JSON_VALUE
                ],
                contentType: MediaType.APPLICATION_JSON_VALUE,
                body: reviewRequest
            )
            def actualPostResponse = postResponse.data

        then: 'Verify that the review was created'
            postResponse.status == 201

        when: 'We add the score to the review'
            def putResponse = restClient.put(path: "/api/reviews/${actualPostResponse.id}/score/4.0")
            def actualPutResponse = putResponse.data

        then: 'Verify that the genre was added'
            actualPutResponse.score == '4.0'

        cleanup:
            restClient.delete(path: "/api/reviews/${actualPostResponse.id}")

    }

    def "PUT /api/review/{reviewId}/score/{score} Returns a 404 when the review does not exist"() {

        when:
            def putResponse = restClient.put(path: "/api/reviews/NonExistent/score/4.0")
            def putResponseBody = putResponse.data

        then:
            putResponse.status == 404
            putResponseBody.message == 'The review with reviewId=NonExistent was not found.'

    }

}
