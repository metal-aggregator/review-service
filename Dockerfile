FROM openjdk:11
COPY ./build/libs/* ./opt/review-service/lib/app.jar
WORKDIR /opt/review-service/lib
ENTRYPOINT ["/usr/local/openjdk-11/bin/java"]
CMD ["-jar", "app.jar"]
EXPOSE 6661
